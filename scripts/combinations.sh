#!/bin/bash

LIST=`grep -l 'stopPoint' $* | xargs`
for i in $LIST
do
    if [ `echo $i | grep '[0-9]*-[0-9]*-[0-9]*'` ]
    then
        i=`basename $i .xml`
        STOP=`echo $i | cut -d '-' -f 1`
        BUS=`echo $i | cut -d '-' -f 2`
        DEST=`echo $i | cut -d '-' -f 3`
        echo "BUS $BUS passes through $STOP while going to $DEST"
    fi
done
