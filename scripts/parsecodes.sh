#!/bin/bash

PARTIALFILE=/tmp/partialist.txt
CODELISTFILE=./codelist.txt
ROUGHLIST=`cat $* | grep -A 2 'stopPoint' | grep -e '\(id\|name\)' | sed 's <[^>]*>  g'`
ROUGHLIST=`echo $ROUGHLIST | xargs`

rm -f $PARTIALFILE
touch $PARTIALFILE

for i in $ROUGHLIST
do
    ID=`echo $i | grep '^[0-9]*$'`
    if [ $ID ]
    then
        echo -ne "\n$ID\t" >>$PARTIALFILE
    else
        NAME=$i
        echo -ne $NAME" "  >>$PARTIALFILE
    fi
done


cat $CODELISTFILE $PARTIALFILE | sort -n | uniq | tee $CODELISTFILE
