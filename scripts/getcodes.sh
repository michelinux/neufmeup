#!/bin/bash

BASEURI='http://envibus.tsi.cityway.fr/CasaSIRIClient/GetStopTimetable.aspx?uID=CA06SA&filterKeys='
AMPHORES=453
GVSA=254
for dest in $GVSA
do
    echo "=== Destination $dest ==="
    for stop in `seq 1 1000`
    do
        echo -n "- Getting page for STOP=$stop and for BUSES "
        for bus in 1
        do
	    	echo -n $bus" "
            
	    	REQUEST=${stop}'$'${bus}'$'${dest}
	    	OUTFILE=`echo $REQUEST | sed 's/\\$/-/g'`.xml
            wget ${BASEURI}$REQUEST -O $OUTFILE -a wget.log
            dos2unix $OUTFILE &>/dev/null

	    done #bus
        echo " Done!"
    	#Sleep randomly between 0 and 9 seconds
    	sleep `echo $RANDOM | grep -o '[0-9]$'`
    done #stop
done #dest
