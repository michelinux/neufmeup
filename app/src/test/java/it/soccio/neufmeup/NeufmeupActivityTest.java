
package it.soccio.neufmeup;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import android.content.res.Resources.NotFoundException;

import static it.soccio.neufmeup.R.*;

public class NeufmeupActivityTest extends BaseTestCase {

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        // For these tests we will give a fake page. Need not to access to Internet
        NeufmeupActivity.setBlockInternetAccess(true);
    }

    public void testGetBuses() throws NotFoundException, IOException, IllegalArgumentException,
            IllegalAccessException, InvocationTargetException, NoSuchFieldException, NoSuchMethodException {

        // Access to mBuses
        ArrayList<Bus> buses = access_mBuses(getActivity());

        String page = readFile(mResources.openRawResource(R.raw.page_brucs_december2012));
        // Access and invoke the "getBuses" method
        Method m;
        m = NeufmeupActivity.class.getDeclaredMethod("getBuses", new Class[] {
                String.class
        });
        m.setAccessible(true);
        m.invoke(getActivity(), new Object[] {
                page
        });

        // CHECK - There are two buses we want to check in this page
        int minutesFoundCounter = 0;
        for (Bus bus : buses) {
            if (bus.number != 0 && bus.minutes.isEmpty() == false)
                ++minutesFoundCounter;
        }
        if (minutesFoundCounter != 2) {
            fail("Expected to read two buses, but we read " + minutesFoundCounter);
        }

        // CHECK - I want to find the '1' ...
        String expectedMinutes = "#1: 4 minutes";
        if (checkIfContains(buses, 1, expectedMinutes) == false)
            fail("Couldn't find the minutes " + expectedMinutes);

        // CHECK - ... and the '11'
        expectedMinutes = "#11: 26 minutes";
        if (checkIfContains(buses, 11, expectedMinutes) == false)
            fail("Couldn't find the minutes " + expectedMinutes);
    }

    public void testAddFakeData() throws IllegalArgumentException, NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        // Access to mBuses
        ArrayList<Bus> buses = access_mBuses(getActivity());
        for (Bus bus : buses) {
            bus.minutes = "";
        }

        // Access and invoke the "addFakeData" method
        final Method m;
        m = NeufmeupActivity.class.getDeclaredMethod("addFakeData");
        m.setAccessible(true);
        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                try {
                    m.invoke(getActivity());
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });
        getInstrumentation().waitForIdleSync();
        // m.invoke(getActivity(), (Object[]) null );

        for (Bus bus : buses) {
            switch (bus.number) {
                case 1:
                case 11:
                    if (bus.minutes.isEmpty())
                        fail("The minutes were not filled for the bus #" + bus.number);
                    break;

                default:
                    if (bus.minutes.isEmpty() == false)
                        fail("Wasn't expecting any minutes or error code for bus #" + bus.number);
                    break;
            }
        }
    }

    @Override
    protected void tearDown() throws Exception {
        NeufmeupActivity.setBlockInternetAccess(false);
        super.tearDown();
    }


    private static boolean checkIfContains(ArrayList<Bus> buses, int busNumber, String expectedMinutes) {
        for (Bus bus : buses) {
            if (bus.number == busNumber && bus.minutes.equals(expectedMinutes)) {
                return true;
            }
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    private static ArrayList<Bus> access_mBuses(NeufmeupActivity activity) throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        ArrayList<Bus> buses;
        Field f = NeufmeupActivity.class.getDeclaredField("mBuses");
        f.setAccessible(true);
        buses = (ArrayList<Bus>) f.get(activity);
        return buses;

    }
}
