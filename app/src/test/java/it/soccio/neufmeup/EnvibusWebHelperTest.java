package it.soccio.neufmeup;

import java.io.IOException;

public class EnvibusWebHelperTest extends BaseTestCase {


    public void testGetNextMinutes() throws IOException {
        final String page_8 = readFile(mResources.openRawResource(R.raw.page_8));
        final String page_brucs_december2012 = readFile(mResources.openRawResource(R.raw.page_brucs_december2012));

        if (!EnvibusWebHelper.getNextMinutes(page_8, 8, "VILLA CHRETIEN").equals("9 minutes"))
            fail();

        if (!EnvibusWebHelper.getNextMinutes(page_brucs_december2012, 11, "GARE SNCF D'ANTIBES").equals("26 minutes"))
            fail();
        
        if (EnvibusWebHelper.getNextMinutes(page_brucs_december2012,  11, "Cagnano Varano").isEmpty() == false) {
            fail("The bus #11 does not go to Cagnano Varano!");
        }
    }
}