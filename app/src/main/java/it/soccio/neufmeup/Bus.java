package it.soccio.neufmeup;

import it.soccio.android.logmeup.lib.SLog;

class Bus {
    private static final String TAG = "BUS";

    int number;
    String direction;
    String minutes = "";

    Bus(int number, String direction) {
        this.number = number;
        this.direction = direction;
    }

    /**
     * Parse the minutes from a string like "#11: 26 minutes".
     * 
     * @param s something like "#11: 26 minutes"
     * @return the minutes from the string or '-1' if it was not possible to parse the string
     */
    public static int parseMinutes(String s) {
        // Extract the number just in front of "minutes"
        int pos = s.indexOf("minutes") - 3;
        if (pos==-1) {
            SLog.e(TAG, "The String " + s +" does not contain 'minutes'");
            return -1;
        }
        String minutesString = "";
        if (s.charAt(pos) == ' ') //trim white space in case of one digit minutes
            ++pos;
        while (Character.isDigit(s.charAt(pos))) {
            minutesString += s.charAt(pos++);
        }

        // Convert the number in an int
        int minutes = -1;
        try {
            minutes = Integer.parseInt(minutesString);
        } catch (NumberFormatException e) {
            SLog.e(TAG, "Wasn't possible parse minutes from string " + s + "\n" + e);
        }
        return minutes;
    }

}
