package it.soccio.neufmeup;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import it.soccio.android.logmeup.lib.SLog;

public class NeufmeupActivity extends Activity  implements SensorEventListener{

    ArrayList<Bus> mBuses = new ArrayList<Bus>();

    private static final String TAG = "NeufmeupActivity";

    /* For Unit Test purposes only */
    private static boolean mBlockInternetAccess = false;

    static final int RefreshInterval = 30000 /* ms */;
    //static String URL_GEN_BRUCS = "http://tempsreel.envibus.fr/hour/?ptano=351%24BRUCS&all=&ligno=351%241%24453&ligno=351%2412%241174&ligno=351%2422%24255&ligno=351%2422%242509";
    static String URL_GEN_BRUCS = "http://tempsreel.envibus.fr/hour/?ptano=351%24BRUCS&ligno=351%241%24453&ligno=351%2413%242840";
    private Timer mAutoUpdate;
    private TimerTask mTimerTask;

    // GUI
    private LinearLayout mMainLayout;
    private TextView mRefreshTextView;

    /* ************************** Shake Detection Variables ****************************** */
    private SensorManager mSensors;
    private float mAccelCurrent = SensorManager.GRAVITY_EARTH;
    private float mAccelLast = SensorManager.GRAVITY_EARTH;
    private float mAccel = 0;
    /* *********************************************************************************** */

    private boolean mIsRefreshing = false;

    private int mCurrentBus = 0;


    /* *********************************************************************************** */

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SLog.v(TAG, "onCreate");

        // Setup the GUI
        setContentView(R.layout.activity_neufmeup);
        mRefreshTextView = (TextView) findViewById(R.id.RefreshingTextView);
        mMainLayout = (LinearLayout) findViewById(R.id.MainLayout);

        // Start the logger
        SLog.StartFileLogging(this);

        // Set the timer for the auto refresh
        mAutoUpdate = new Timer();
        mTimerTask = new TimerTask() {
            @Override
            public void run() {
                update();
            }
        };
        mAutoUpdate.schedule(mTimerTask, 0, RefreshInterval);

        // This will fill the list of buses we want
        fillBuses();

        // Setup the timeouts, one third for the connect timeout, two thirds for
        // the read timeout
        // Both will have a margin of 500 ms
        EnvibusWebHelper.setConnectTimeout(RefreshInterval / 3 - 500);
        EnvibusWebHelper.setReadTimeout(RefreshInterval / 3 * 2 - 500);

        // updateMenuVisibility();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SLog.v(TAG, "onResume");

        // Start motion detection
        if (mSensors == null)
            mSensors = (SensorManager) getSystemService(SENSOR_SERVICE);

        mSensors.registerListener(this,
                mSensors.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        // Calling the parent method adds the menu even if I don't have the actionbar
        super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.menu_neufmeup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.menu_fake_data:
                addFakeData();
                updateGUI();
                break;
            case R.id.refresh:
                SLog.d(TAG, "Menu Refresh");
                mTimerTask.cancel();
                mTimerTask = new TimerTask() {
                    @Override
                    public void run() {
                        update();
                        updateGUI();
                    }
                };
                mAutoUpdate.schedule(mTimerTask, 0, RefreshInterval);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Stop motion detection
        mSensors.unregisterListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mSensors != null)
            mSensors = null;

        SLog.d(TAG, "onPause. Cancelling the mAutoUpdate timer");
        mTimerTask.cancel();
        mAutoUpdate.purge();

        // Stop the logger
        SLog.StopFileLogging();
    }

    private synchronized boolean update() {
        if (mBlockInternetAccess) {
            SLog.i(TAG, "Internet access is blocked. Skipping update() method");
            return false;
        }

        SLog.i(TAG, "Getting the page from Envibus and updating the GUI");
        String url = URL_GEN_BRUCS;
        String webPage = "";
        // ArrayList<String> buses = null;

        // Tell the user the app is refreshing
        setRefreshing(true);

        // Poll the website
        SLog.d(TAG, "update: Polling Web Page" + url);
        try {
            webPage = EnvibusWebHelper.executeHttpGet(url);
        } catch (Exception e) {
            SLog.e(TAG, "Something has gone wrong while getting the webPage from the Envibus Website");
            SLog.w(TAG, e.getStackTrace().toString());
            e.printStackTrace();
            setRefreshing(false);
            return false;
        }

        // Parse the website
        getBuses(webPage);

        // We are not refreshing anymore
        setRefreshing(false);

        // Everything seems OK
        return true;
    }

    private void updateGUI() {
        // Clean the previous screen
        mMainLayout.removeAllViewsInLayout();

        // Add the results on the screen, one new BusView for each bus with
        // minutes
        final LinearLayout l = mMainLayout;
        for (final Bus bus : mBuses) {
            // Skip all the buses that are empty
            if (bus.minutes.isEmpty() == false) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        BusView bv = BusView.getNewBusView(bus.number, NeufmeupActivity.this/*, NeufmeupActivity.this*/);
                        bv.setText(bus.minutes);
                        registerForContextMenu(bv);
                        l.addView(bv);
                    }
                });
            }
        }
    }

    private void getBuses(String webPage) {
        // Start from scratch
        // ArrayList<String> times = new ArrayList<String>();
        for (Bus b : mBuses)
            b.minutes = "";

        boolean atLeastOneBus = false;

        try {
            // Get the buses
            String s = "";
            for (Bus bus : mBuses) {
                // Skip the fake number 0, used for errors
                if (bus.number != 0) {

                    s = EnvibusWebHelper.getNextMinutes(webPage, bus.number, bus.direction);
                    if (s.isEmpty() == false) {
                        // times.add("#" + bus.number + ": " + s);
                        bus.minutes = "#" + bus.number + ": " + s;
                        atLeastOneBus = true;
                    }
                }
            }

            if (atLeastOneBus == false) {
                for (Bus bus : mBuses) {
                    if (bus.number == 0) {
                        bus.minutes = "No Fucking Bus!";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            // times.add("Error while connecting to envibus.fr");
            for (Bus bus : mBuses) {
                if (bus.number == 0) {
                    bus.minutes = "Error while connecting to envibus.fr";
                }
            }
        }

        // return times;
    }

    private void setRefreshing(boolean refreshing) {
        final String s = refreshing ? "Refreshing..." : "";
        mIsRefreshing = refreshing;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mRefreshTextView.setText(s);
            }
        });
    }

    public void onSensorChanged(SensorEvent se) {
        if (mIsRefreshing) {
            return;
        }

        float x = se.values[0];
        float y = se.values[1];
        float z = se.values[2];
        mAccelLast = mAccelCurrent;
        mAccelCurrent = x * x + y * y + z * z;
        float delta = mAccelCurrent - mAccelLast;
        mAccel = mAccel * 0.9f + delta; // perform low-cut filter

        if (mAccel > 200.0) {
            SLog.d(TAG, "Shake Detected " + mAccel);
            mTimerTask.cancel();
            mTimerTask = new TimerTask() {
                @Override
                public void run() {
                    update();
                    updateGUI();
                }
            };
            mAutoUpdate.schedule(mTimerTask, 0, RefreshInterval);
        }
    }

    private void fillBuses() {
        mBuses.add(new Bus(0, "ERROR FAKE BUS"));
        mBuses.add(new Bus(1, "AMPHORES"));
        mBuses.add(new Bus(9, ""));
        mBuses.add(new Bus(12, "PASSERELLE SNCF"));
        mBuses.add(new Bus(12, "GARE SNCF D'ANTIBES"));
        mBuses.add(new Bus(12, "ANTIBES"));
        mBuses.add(new Bus(12, ""));
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // This method is not used
    }

    /**
     * This method is for test purposes only, and should not appear in the
     * released version of the application!
     */
    private void addFakeData() {
        Random random = new Random();

        // Clear
        mMainLayout.removeAllViews();

        // Add the fake buses
        for (Bus bus : mBuses) {
            int fakeMinutes = random.nextInt(28) + 1;
            switch (bus.number) {
                case 1:
                case 11:
                    bus.minutes = "#" + bus.number + ": " + fakeMinutes + " minutes";
                    break;

                default:
                    bus.minutes = "";
                    break;
            }
        }
    }

    /**
     * For Unit Test use only: it will avoid Neuf me up connect to the internet, so that the fake data
     * used by the test won't be suddenly overwritten by real data coming from the network.
     * @param access <b>true</b> if you want to block internet, <b>false</b> to restore normal behavior
     */
    protected static void setBlockInternetAccess(boolean access) {
        mBlockInternetAccess = access;
    }
}
