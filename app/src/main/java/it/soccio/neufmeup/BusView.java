package it.soccio.neufmeup;

import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

public class BusView extends TextView {

    protected int busNumber;

    public BusView(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
    }
    
    static public BusView getNewBusView(int busNumber, Context context/*, OnLongClickListener onLongClickListener*/) {

        //Create the view
        BusView busView = new BusView(context);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 0, 1f);
        busView.setLayoutParams(params);
        busView.setGravity(Gravity.CENTER);
        busView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 45);
        busView.setTypeface(Typeface.DEFAULT_BOLD);

/*        //Set the listener
        busView.setOnLongClickListener(onLongClickListener);*/
        
        //Assign the bus number
        busView.busNumber = busNumber;
        
        //Done!
        return busView;
    }
}
