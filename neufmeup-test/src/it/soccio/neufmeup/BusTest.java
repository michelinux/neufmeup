package it.soccio.neufmeup;

import junit.framework.TestCase;

public class BusTest extends TestCase {

    protected void setUp() throws Exception {
        super.setUp();
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testParseMinutes() {
        int minutes = Bus.parseMinutes("#11: 26 minutes");
        if (minutes != 26) {
            fail("Expected 26 minutes but parsed " + minutes);
        }
        
        minutes = Bus.parseMinutes("#9: 2 minutes");
        if (minutes != 2) {
            fail("Expected 2 minutes but parsed " + minutes);
        }
    }

}
