package it.soccio.neufmeup;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.content.res.Resources;
import android.test.ActivityInstrumentationTestCase2;

public class BaseTestCase extends ActivityInstrumentationTestCase2<NeufmeupActivity>{

    protected Resources mResources;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mResources = getInstrumentation().getContext().getResources();
    }
    
    /**
     * Reads a file from an InputStream, for example a text file in the raw resources and put it
     * into a String ready to be used.
     * Example<br/> <code> 
     * Resources resources = getInstrumentation().getContext().getResources();<br/>
     * String s = readFile(resources.openRawResource(R.raw.myfile);
     * </code>
     * 
     * @param stream
     * @return a String with the content of the stream
     * @throws IOException
     */
    protected static String readFile(InputStream stream) throws IOException {
        String result = "";
        DataInputStream in = new DataInputStream(stream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String strLine;
    
        // Read File Line By Line
        while ((strLine = br.readLine()) != null) {
            result += strLine;
        }
        in.close();
    
        return result;
    }

    public BaseTestCase() {
        super(NeufmeupActivity.class);
    }

}
