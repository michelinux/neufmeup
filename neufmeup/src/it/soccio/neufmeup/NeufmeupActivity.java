package it.soccio.neufmeup;

import it.soccio.android.logmeup.lib.SLog;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;

public class NeufmeupActivity extends Activity implements SensorEventListener, OnLongClickListener {

    ArrayList<Bus> mBuses = new ArrayList<Bus>();

    private static final String TAG = "NeufmeupActivity";

    /* For Unit Test purposes only */
    private static boolean mBlockInternetAccess = false;

    static final int RefreshInterval = 30000 /* ms */;
    static String URL_GEN_BRUCS = "http://tempsreel.envibus.fr/hour/?ptano=351%24BRUCS&all=&ligno=351%241%24453&ligno=351%2412%241174&ligno=351%2422%24255&ligno=351%2422%242509";

    private Timer mAutoUpdate;
    private TimerTask mTimerTask;

    // GUI
    private LinearLayout mMainLayout;
    private TextView mRefreshTextView;

    /* ************************** Shake Detection Variables ****************************** */
    private SensorManager mSensors;
    private float mAccelCurrent = SensorManager.GRAVITY_EARTH;
    private float mAccelLast = SensorManager.GRAVITY_EARTH;
    private float mAccel = 0;
    /* *********************************************************************************** */

    private boolean mIsRefreshing = false;

    /* ****************************** Facebook stuff ************************************* */
    Session mFacebookSession;
    SessionState mFacebookSessionState;

    // Create the callback to handle the response
    Request.Callback mFacebookRequestCallback = new Request.Callback() {
        public void onCompleted(Response response) {
            FacebookRequestError error = null;
            error = response.getError();
            if (error != null) {
                SLog.e(TAG, "There was an error while posting the story: " + error.getErrorCode() + " " + error.getErrorMessage());
                return;
                // Toast.makeText(getActivity().getApplicationContext(),
                // error.getErrorMessage(), Toast.LENGTH_SHORT).show();
            } else {
                SLog.i(TAG, "Story was posted successfully");
            }

            GraphObject graphObject = response.getGraphObject();
            JSONObject graphResponse = graphObject.getInnerJSONObject();
            try {
                SLog.i(TAG, "Completed Facebook Request: GraphResponse id: " + graphResponse.getString("id"));
            } catch (JSONException e) {
                SLog.e(TAG, "JSON error " + e.getMessage());
            }

        }
    };

    /* *********************************************************************************** */

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SLog.v(TAG, "onCreate");

        // Setup the GUI
        setContentView(R.layout.main);
        mRefreshTextView = (TextView) findViewById(R.id.RefreshingTextView);
        mMainLayout = (LinearLayout) findViewById(R.id.MainLayout);

        // Start the logger
        SLog.StartFileLogging(this);

        // Set the timer for the auto refresh
        mAutoUpdate = new Timer();
        mTimerTask = new TimerTask() {
            @Override
            public void run() {
                update();
            }
        };
        mAutoUpdate.schedule(mTimerTask, 0, RefreshInterval);

        // This will fill the list of buses we want
        fillBuses();

        // Setup the timeouts, one third for the connect timeout, two thirds for
        // the read timeout
        // Both will have a margin of 500 ms
        EnvibusWebHelper.setConnectTimeout(RefreshInterval / 3 - 500);
        EnvibusWebHelper.setReadTimeout(RefreshInterval / 3 * 2 - 500);

        // Show username
        retrieveFacebookUser(Session.getActiveSession());
        // updateMenuVisibility();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SLog.v(TAG, "onResume");

        // Start motion detection
        if (mSensors == null)
            mSensors = (SensorManager) getSystemService(SENSOR_SERVICE);

        mSensors.registerListener(this,
                mSensors.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_fake_data:
                addFakeData();
                updateGUI();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * 
     */
    private void loginToFacebook() {
        mFacebookSession = new Session(this);
        mFacebookSession.addCallback(new Session.StatusCallback() {
            // callback when session changes state
            @Override
            public void call(Session session, SessionState state, Exception exception) {
                mFacebookSession = session;
                retrieveFacebookUser(session);
            }
        });
        ArrayList<String> permissions = new ArrayList<String>();
        permissions.add("publish_stream");
        Session.OpenRequest openRequest = new Session.OpenRequest(this);
        openRequest.setPermissions(permissions);
        openRequest.setCallback(new Session.StatusCallback() {
            // callback when session changes state
            @Override
            public void call(Session session, SessionState state, Exception exception) {
                mFacebookSession = session;
                retrieveFacebookUser(session);
            }
        });
        mFacebookSession.openForPublish(openRequest);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Stop motion detection
        mSensors.unregisterListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mSensors != null)
            mSensors = null;

        SLog.d(TAG, "onPause. Cancelling the mAutoUpdate timer");
        mTimerTask.cancel();
        mAutoUpdate.purge();

        // Stop the logger
        SLog.StopFileLogging();
    }

    private synchronized boolean update() {
        if (mBlockInternetAccess) {
            SLog.i(TAG, "Internet access is blocked. Skipping update() method");
            return false;
        }

        SLog.i(TAG, "Getting the page from Envibus and updating the GUI");
        String url = URL_GEN_BRUCS;
        String webPage = "";
        // ArrayList<String> buses = null;

        // Tell the user the app is refreshing
        setRefreshing(true);

        // Poll the website
        SLog.d(TAG, "update: Polling Web Page" + url);
        try {
            webPage = EnvibusWebHelper.executeHttpGet(url);
        } catch (Exception e) {
            SLog.e(TAG, "Something has gone wrong while getting the webPage from the Envibus Website");
            SLog.w(TAG, e.getStackTrace().toString());
            e.printStackTrace();
            setRefreshing(false);
            return false;
        }

        // Parse the website
        getBuses(webPage);

        // We are not refreshing anymore
        setRefreshing(false);

        // Everything seems OK
        return true;
    }

    private void updateGUI() {
        // Clean the previous screen
        mMainLayout.removeAllViewsInLayout();

        // Add the results on the screen, one new BusView for each bus with
        // minutes
        final LinearLayout l = mMainLayout;
        for (final Bus bus : mBuses) {
            // Skip all the buses that are empty
            if (bus.minutes.isEmpty() == false) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        BusView bv = BusView.getNewBusView(bus.number, NeufmeupActivity.this, NeufmeupActivity.this);
                        bv.setText(bus.minutes);
                        l.addView(bv);
                    }
                });
            }
        }
    }

    private void getBuses(String webPage) {
        // Start from scratch
        // ArrayList<String> times = new ArrayList<String>();
        for (Bus b : mBuses)
            b.minutes = "";

        boolean atLeastOneBus = false;

        try {
            // Get the buses
            String s = "";
            for (Bus bus : mBuses) {
                // Skip the fake number 0, used for errors
                if (bus.number != 0) {

                    s = EnvibusWebHelper.getNextMinutes(webPage, bus.number, bus.direction);
                    if (s.isEmpty() == false) {
                        // times.add("#" + bus.number + ": " + s);
                        bus.minutes = "#" + bus.number + ": " + s;
                        atLeastOneBus = true;
                    }
                }
            }

            if (atLeastOneBus == false) {
                for (Bus bus : mBuses) {
                    if (bus.number == 0) {
                        bus.minutes = "No Fucking Bus!";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            // times.add("Error while connecting to envibus.fr");
            for (Bus bus : mBuses) {
                if (bus.number == 0) {
                    bus.minutes = "Error while connecting to envibus.fr";
                }
            }
        }

        // return times;
    }

    private void setRefreshing(boolean refreshing) {
        final String s = refreshing ? "Refreshing..." : "";
        mIsRefreshing = refreshing;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mRefreshTextView.setText(s);
            }
        });
    }

    public void onSensorChanged(SensorEvent se) {
        if (mIsRefreshing) {
            return;
        }

        float x = se.values[0];
        float y = se.values[1];
        float z = se.values[2];
        mAccelLast = mAccelCurrent;
        mAccelCurrent = x * x + y * y + z * z;
        float delta = mAccelCurrent - mAccelLast;
        mAccel = mAccel * 0.9f + delta; // perform low-cut filter

        if (mAccel > 200.0) {
            SLog.d(TAG, "Shake Detected " + mAccel);
            mTimerTask.cancel();
            mTimerTask = new TimerTask() {
                @Override
                public void run() {
                    update();
                    updateGUI();
                }
            };
            mAutoUpdate.schedule(mTimerTask, 0, RefreshInterval);
        }
    }

    private void fillBuses() {
        mBuses.add(new Bus(0, "ERROR FAKE BUS"));
        mBuses.add(new Bus(1, "AMPHORES"));
        mBuses.add(new Bus(9, ""));
        mBuses.add(new Bus(11, "PASSERELLE SNCF"));
        mBuses.add(new Bus(11, "GARE SNCF D'ANTIBES"));
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // This method is not used
    }

    protected void tellFacebookConnect(String user, String status) {
        final String s = "User: " + user + ", status: " + status;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mRefreshTextView.setText(s);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        /* Tell Facebook Session */
        if (mFacebookSession != null) {
            mFacebookSession.onActivityResult(this, requestCode, resultCode, data);
        }
    }

    private void retrieveFacebookUser(Session session) {
        if (session == null) {
            SLog.d(TAG, "There is no Facebook Session");
            return;
        }
        if (session.isClosed()) {
            SLog.d(TAG, "There is a Facebook Session, but it is Closed");
            return;
        }
        if (session.isOpened()) {
            SLog.d(TAG, "There is a Facebook Session and it is Open");

            // make request to the /me API
            Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {

                // callback after Graph API response with user object
                @Override
                public void onCompleted(GraphUser user, Response response) {
                    if (user != null) {
                        mRefreshTextView.setText(user.getName() + " is logged in");
                    } else {
                        mRefreshTextView.setText("No Login to Facebook. Retry");
                    }
                }
            });
        }
    }

    /* The correct way, by using the Open Graph API */
    private void takeTheBus(int busNumber) {
        // If no session, no worth continue
        if (mFacebookSession == null) {
            SLog.e(TAG, "Cannot share if there is no Facebook Session");
            return;
        }

        // Find that bus
        Bus bus = null;
        for (Bus b : mBuses) {
            if (b.number == busNumber) {
                bus = b;
                break;
            }
        }
        final int minutes = Bus.parseMinutes(bus.minutes);

        // Prepare parameters
        Bundle params = new Bundle();
        params.putString("bus", "http://www.soccio.it/neufmeup/fb/bus.html");
        params.putString("message", "@[1524177013] Je prends le bus dans " + minutes + " minutes");
        params.putString("tags", "1524177013");

        // Prepare request
        SLog.i(TAG, "Telling Benjamin I'm taking the bus in " + minutes + " minutes");
        Request request = new Request(mFacebookSession, "me/neufmeup:be_going_to_take", params, HttpMethod.POST, mFacebookRequestCallback);

        // Send the request
        RequestAsyncTask task = new RequestAsyncTask(request);
        task.execute();
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onLongClick(View v) {
        if (v.getClass() == BusView.class) {
            BusView busView = (BusView) v;
            if (busView.busNumber == 0) {
                SLog.i(TAG, "Long Click on an Error");
            } else {
                SLog.i(TAG, "Long Click on the bus #" + busView.busNumber);
                showSharePopupMenu(v);
            }
            return true;
        }
        return false;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void showSharePopupMenu(View v) {
        PopupMenu popup = new PopupMenu(this, v);

        // If the type is BusView, then assign to bv, bv stays null otherwise
        final BusView bv = (v.getClass() == BusView.class) ? (BusView) v : null;

        // Who will take care of my clicks?
        PopupMenu.OnMenuItemClickListener clickListener = new PopupMenu.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.share:
                        SLog.i(TAG, "Login to Facebook");
                        loginToFacebook();
                        SLog.i(TAG, "Let's share the news about Bus #" + bv.busNumber);
                        takeTheBus(bv.busNumber);
                        break;
                }

                return false;
            }
        };
        popup.setOnMenuItemClickListener(clickListener);

        // Who will tell the popup what are the items of the menu?
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.popup, popup.getMenu());

        popup.show();
    }

    /**
     * This method is for test purposes only, and should not appear in the
     * released version of the application!
     */
    private void addFakeData() {
        Random random = new Random();

        // Clear
        mMainLayout.removeAllViews();

        // Add the fake buses
        for (Bus bus : mBuses) {
            int fakeMinutes = random.nextInt(28) + 1;
            switch (bus.number) {
                case 1:
                case 11:
                    bus.minutes = "#" + bus.number + ": " + fakeMinutes + " minutes";
                    break;

                default:
                    bus.minutes = "";
            }
        }
    }

    /**
     * For Unit Test use only: it will avoid Neuf me up connect to the internet, so that the fake data
     * used by the test won't be suddenly overwritten by real data coming from the network. 
     * @param access <b>true</b> if you want to block internet, <b>false</b> to restore normal behavior
     */
    protected static void setBlockInternetAccess(boolean access) {
        mBlockInternetAccess = access;
    }
}
