package it.soccio.neufmeup;

import it.soccio.android.logmeup.lib.SLog;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class EnvibusWebHelper {
    private static final String TAG = "EnvibusWebHelper";

    // Default values
    private static int mConnectTimeout = 10000 /* ms */;
    private static int mReadTimeout = 10000 /* ms */;

    protected static String getNextMinutes(String page, int lineNumber, String direction) {
        String line = "alt=\"Ligne " + lineNumber + "\"";
        direction = "direction <b>" + direction + "</b>";
        String endString = "</span>";

        String cut = "";
        /* Find the line */
        boolean found = false;
        int i = page.indexOf(line);
        while (found == false && i != -1) {
            int end = page.indexOf(endString, i);
            cut = page.substring(i, end);
            found = cut.indexOf(direction) != -1;
            if (!found) {
                i = page.indexOf(line, end);
            }
        }

        if (i == -1) {
            // Nothing was found
            return "";
        }

        /* At this point I have the string with the line, the direction and the minutes */
        Pattern pattern = Pattern.compile("[0-9]* minutes*");
        Matcher matcher = pattern.matcher(cut);

        String ret = "";
        if (matcher.find()) {
            SLog.d(TAG, String.format("I found the text \"%s\" starting at index %d and ending at index %d.%n",
                    matcher.group(), matcher.start(), matcher.end()));
            ret = matcher.group();
        }

        return ret;
    }

    public static String executeHttpGet(String uri) throws IOException {
        String page = "";
        URL url = new URL(uri);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setConnectTimeout(mConnectTimeout);
        urlConnection.setReadTimeout(mReadTimeout);
        SLog.v(TAG, "Connection Timeout: " + urlConnection.getConnectTimeout() + "ms - Reader Timeout: "
                + urlConnection.getReadTimeout() + "ms");
        try {
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            page = readStream(in);
        } catch (SocketTimeoutException e) {
            SLog.w(TAG, "ConnectException, probably a timeout at connect time.");
            e.printStackTrace();
        } finally {
            urlConnection.disconnect();
        }

        return page;
    }

    /**
     * Read a string from an Input Stream
     * 
     * @param stream
     * @return
     * @throws IOException
     */
    private static String readStream(InputStream stream) throws IOException {
        /* We will read from bytes, then copy it into the String */
        byte[] buffer = new byte[0x1000];
        int bytesRead = 0;
        String content = "";

        while ((bytesRead = stream.read(buffer)) != -1) {
            content += new String(buffer, 0, bytesRead);
        }

        return content;
    }

    public static InputStream getInputStreamFromUrl(String url) {
        InputStream content = null;
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response = httpclient.execute(new HttpGet(url));
            content = response.getEntity().getContent();
        } catch (Exception e) {
            SLog.d("[GET REQUEST]", "Network exception", e);
        }
        return content;
    }

    protected static void setConnectTimeout(int timeout) {
        if (timeout < 0)
            return;
        mConnectTimeout = timeout;
    }

    protected static void setReadTimeout(int timeout) {
        if (timeout < 0)
            return;
        mReadTimeout = timeout;
    }
}
